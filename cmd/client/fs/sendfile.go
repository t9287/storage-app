package fs

import (
	"github.com/skullkon/tcp_file/pkg/logger"
	"io"
	"net"
	"os"
	"strconv"
)

func SendFile(conn net.Conn, fname string, cmd string) {
	fi, err := os.Stat("./" + fname)
	conn.Write([]byte(cmd + "\n"))
	conn.Write([]byte(fname))
	conn.Write([]byte("\n" + strconv.FormatInt(fi.Size(), 10)))

	file, err := os.Open(fname)
	if err != nil {

		return
	}

	defer conn.Close()
	_, err = io.Copy(conn, file)
	if err != nil {
		return
	}
	//_, err = io.Copy(os.Stdout, conn)
	logger.Log(conn, fname, string(fi.Size()), true)
}
