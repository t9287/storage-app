package logger

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"net"
	"os"
	"time"
)

var (
	file *os.File
	err  error
)

// Log status == 0 отправка, status == 1 получение

func LogError(conn net.Conn, fName string, fSize string, status bool, err error) {
	defer conn.Close()
	log := fmt.Sprintf("%s  %s  %s  %t  %v", time.Now().String(), fName, fSize, status, err.Error())
	file.Write([]byte(log))
	defer conn.Close()
}

func Log(conn net.Conn, fName string, fSize string, status bool) {
	fmt.Println("логгер")
	defer conn.Close()
	file, err = os.OpenFile("./log.txt", os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	if err != nil {
		logrus.Error(err)
	}
	log := fmt.Sprintf("%s  %s successfully transmitted Size = %s byte Status %t \n", time.Now().String(), fName, fSize, status)
	_, err := file.WriteString(log)
	if err != nil {
		fmt.Println(err)
		return
	}

}
