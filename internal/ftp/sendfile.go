package ftp

import (
	"fmt"
	"github.com/skullkon/tcp_file/pkg/logger"
	"io"
	"log"
	"net"
	"os"
)

func SendFile(conn net.Conn, fName string) {
	defer conn.Close()
	file, err := os.Open(root + fName)
	l, err := file.Stat()
	if err != nil {
		//logger.Log(conn,fName,f)
		return
	}
	defer file.Close()
	fmt.Println(l.Name())
	_, err = io.Copy(conn, file)
	if err != nil {
		log.Println(err)
		return
	}

	logger.Log(conn, fName, string(l.Size()), false)

}
