package ftp

import (
	"fmt"
	"github.com/google/uuid"
	"github.com/skullkon/tcp_file/pkg/logger"
	"io"
	"log"
	"net"
	"os"
	"path/filepath"
	"strings"
)

var (
	root, _ = filepath.Abs("./fileServer/")
)

func GetFile(conn net.Conn, fName string, fSize string) {
	defer conn.Close()
	uid := uuid.New().String()

	//base, err := db.NewPostgresDB()
	//if err != nil {
	//	log.Println(err)
	//	return
	//}
	//
	//rep := db.NewRepository(base)

	fileDir := root+"/" + uid + "." + strings.Split(fName, ".")[2]

	file, err := os.Create(fileDir)
	if err != nil {
		log.Println(err)
		return
	}
	fmt.Println(fileDir)
	//rep.Add(conn, uid, fileDir)

	defer file.Close()

	_, err = io.Copy(file, conn)
	if err != nil {
		log.Println(err)
		return
	}

	logger.Log(conn, fName, fSize, true)
}
